package com.c0d3in3.lecture12

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), CustomAdapter.CustomInterface{

    private val itemsList = arrayListOf<ItemModel>()
    private val adapter = CustomAdapter(itemsList, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = adapter
        addDataToList()
    }

    private fun addDataToList(){
        itemsList.add(ItemModel(R.mipmap.ic_doggo_1, "doggo 1", "Click image to buy"))
        itemsList.add(ItemModel(R.mipmap.ic_doggo_2, "doggo 2", "Click image to buy"))
        itemsList.add(ItemModel(R.mipmap.ic_doggo_3, "doggo 3", "Click image to buy"))
        itemsList.add(ItemModel(R.mipmap.ic_doggo_4, "doggo 4", "Click image to buy"))
        itemsList.add(ItemModel(R.mipmap.ic_doggo_5, "doggo 5", "Click image to buy"))
        itemsList.add(ItemModel(R.mipmap.ic_doggo_6, "doggo 6", "Click image to buy"))
    }

    override fun checkThis() {
        Toast.makeText(this, "Ha-Ha you fool!", Toast.LENGTH_LONG).show()
    }

}
